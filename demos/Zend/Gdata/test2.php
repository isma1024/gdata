<?php
    set_include_path("/var/www/zendgdata/library/");

    require_once 'Zend/Loader.php';
    Zend_Loader::loadClass('Zend_Gdata_ClientLogin');
    Zend_Loader::loadClass('Zend_Gdata_Spreadsheets');
    Zend_Loader::loadClass('Zend_Gdata_Docs');

/**
 * Username, password and the name of the spreadsheet we would like to use
 * Note that the spreadsheet is case sensitive and white space sensitive
 */

    $user = "ismael.perception@gmail.com";
    $pass = "ismael1234!";
    /*if($_POST['name'])
    {
        $spreadsheetToFind = $_POST['name'];
    }
    elseif($_GET['name'])
    {
        $spreadsheetToFind = $_GET['name'];
    }*/

/**
 * Establish a connection to our spreadsheets and get a complete list of them
 */

    $service            = Zend_Gdata_Spreadsheets::AUTH_SERVICE_NAME;
    $client             = Zend_Gdata_ClientLogin::getHttpClient($user, $pass, $service);
    $spreadsheetService = new Zend_Gdata_Spreadsheets($client);
    $feed               = $spreadsheetService->getSpreadsheetFeed();

/**
 * We loop through all of the spreadsheets until we have found the one
 * declared earlier. The id of the spreadsheet is then extracted using
 * basename and we store the id for use in our next query, to
 * obtain all of the worksheets in the spreadsheet
 */

    foreach($feed->entries as $entry)
    {               
        $spreadsheetTitle = $entry->title->text;

        if($spreadsheetTitle == $spreadsheetToFind)
        {
            $spreadsheetURL = $entry->id;
        }
    }

    $spreadsheetKey = basename($spreadsheetURL);

    $query = new Zend_Gdata_Spreadsheets_DocumentQuery();
    $query->setSpreadsheetKey($spreadsheetKey);
    $feed = $spreadsheetService->getWorksheetFeed($query); // now that we have the desired spreadsheet, we need the worksheets

/**
 * Loop through all of our worksheets and echo
 * its name as well as its id
 */

    echo("<table><tr><td><strong>Spreadsheet Name:</strong></td><td>" . $spreadsheetToFind . "</td></tr><tr><td><strong>Spreadsheet ID:</strong></td><td>" . $spreadsheetKey . "</td></tr>");

    foreach($feed->entries as $entry)
    {
        echo("<tr><td><strong>".$entry->title->text . ": </strong></td><td>".basename($entry->id)."</td></tr>");

    }

    echo("</table>");
?>